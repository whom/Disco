defmodule Disco.MixProject do
  use Mix.Project

  def project do
    [
      app: :disco,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {DiscoSupervisor, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
	    {:nostrum, git: "https://github.com/Kraigie/nostrum.git"},
      {:ex_pwned, "~> 0.1.2"},
      {:httpoison, "~> 1.0", [override: true]}
    ]
  end
end

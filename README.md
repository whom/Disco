# Disco

Disco is a Discord bot that exists to integrate with as many interesting Elixir libraries as possible
At the moment Disco can use 
 - expwned

Since we aim to take advantage of as many libraries as possible feel free to lodge a PR with something interesting.

## Building

You'll need to generate a config file (config/config.exs) containing the following

```
use Mix.Config
config :nostrum,
	token: <<Your Discord app token here>>,
	num_shards: :auto
```

You can then build this bot with the following commands
```
$ mix deps.get
$ mix compile
```

## Running

```
$ mix run --no-halt
```

defmodule DiscoSupervisor do
  require Logger

  def start(_type, _args) do 
    Logger.info "Disco now running!\n" 
    import Supervisor.Spec

    children = [DiscoMessageHandler]
    Supervisor.start_link(children, strategy: :one_for_one)

  end
end

defmodule DiscoMessageHandler do
  use Nostrum.Consumer
  alias Nostrum.Api
  require Logger

  def start_link do
    Consumer.start_link(__MODULE__)
  end

  def handle_event({:MESSAGE_CREATE, {msg}, ws_state}) do
    Logger.debug "Processing Message: " <> msg.content

    case msg.content do
      "!"<>command ->
        Api.create_message(msg.channel_id, content: MessageProcessor.handle(command))
      _ ->
        :ignore
    end
  end

  def handle_event(_event) do
    :noop # think of something better later
  end
end

defmodule MessageProcessor do
  require Logger
  require ExPwned

  def handle(arg) do
    Logger.debug "Processing Command: " <> arg
    case arg do

      "ping" ->
        ping()

      "pwned "<>rest ->
        pwn(rest)

      "bots" ->
        info()

      _ ->
        "Invalid request"

    end
  end

  def ping() do
    "pong"
  end

  def pwn(arg) do
    Logger.debug "Expwned called: "<>arg

    case arg do
      "account "<>rest ->
        ExPwned.breached?(rest)
      "password "<>rest ->
        ExPwned.password_breach_count(rest)
      _ ->
        "Invalid request"
    end
  end

  def info do
    "Hello! I am Disco! you can find my code here: https://github.com/Benevolent/Disco"
  end
end
